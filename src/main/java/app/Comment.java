package app;

/**
 * Represents a comment submitted by a user
 */
public class Comment {
    /**
     * Content of the comment
     */
    private String content;

    public Comment(){
        super();
    }

    public Comment(final String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(final String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return this.content;
    }
}
