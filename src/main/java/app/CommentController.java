package app;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;

@RestController
public class CommentController {

    /**
     * A sudo storage to save comments
     */
    private ArrayList<CommentRecord> storage = new ArrayList<CommentRecord>();

    /**
     * Gets JSON from POST request body of users and
     * stores.
     * @return comment content
     */
    @PostMapping("/submit")
    public String newComment(@RequestBody Comment comment) {
        CommentRecord commentRecord = new CommentRecord();
        commentRecord.setContent(comment.getContent());

        this.storage.add(commentRecord);
        return "Your comment '" + comment + "' has been successfully stored";
    }
}
